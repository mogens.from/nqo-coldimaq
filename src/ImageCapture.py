from PyQt5 import QtCore, QtGui

class ImageCaptureThread(QtCore.QThread):

    imagesReady = QtCore.pyqtSignal(object)

    def __init__(self, cam):
        QtCore.QThread.__init__(self)
        self.cam = cam

    def __del__(self):
        self.wait()

    def run(self):
        imgs = self.cam.startListening()
        self.imagesReady.emit(imgs)

        self.terminate()