# Main GUI for the image acquisition system.
# Mogens Henrik From, September 2019. Written for NQO at SDU, Odense, Denamrk.

import sys
#sys.path.append('D:\\NQO\\YQO\\Our Experimental Control\\Experiment Devices')
#sys.path.append('C:\\Users\\MogensF\\NQO-SVN\\YQO\\Our Experimental Control\\Experiment Devices')
sys.path.append('C:\\Users\\MogensF\\Documents\\Git\\nqo-coldimaq')
from dbaccess.Configuration import DB_USER, DB_PASSWD, DB_HOST, DB_PORT, DB_NAME
from dbaccess.InstrumentServerCommunication import InstrumentServer
from dbaccess.Configuration import PORT_CAMERA, IP_YQOCONTROL, DATA_CAMERA
import mysql.connector
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from ImageCapture import ImageCaptureThread
import threading

import logging

logging.basicConfig(level=logging.INFO)

comms = {'PORT': PORT_CAMERA,
			'DATA': DATA_CAMERA,
			'ADDR': IP_YQOCONTROL}

qt_ui_file = "mainwindow.ui" # Created with Qt Creator
Ui_MainWindow, QtBaseClass = uic.loadUiType(qt_ui_file)

class ImAqGUI(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        # Setting vars:
        self.numImages = 3

        # Vars:
        self.globalCounter = 0
        self.localCounter = 0
        self.threads = []
        self.camIsConnected = False
        self.isGettingImages = False

        # QT Setup:
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.showGlobalCounter.display(self.globalCounter)

        # Button trggers:
        self.startImAq_btn.clicked.connect(lambda: self.readyForAcquisition())
        self.stopImAq_btn.clicked.connect(lambda: self.stopAcquisition())
        self.getSnapshot_btn.clicked.connect(lambda: self.takeSnapshot())
        # Grey out unconnected cameras?
        # Grey out "stop" button

        # Setup
        self.connectToDatabase()
        self.updateGlobalCount()

    def __del__(self):
        self.disconnectFromDatabase()
        # Disconnect camera, if already connected:
        try:
            del self.cam
        except AttributeError:
            pass

    def connectToDatabase(self):
        try:
            self.db = mysql.connector.connect(user=DB_USER, password=DB_PASSWD, host=DB_HOST, port=DB_PORT,
                                                 database=DB_NAME)
        except Exception as e:
            logging.debug('Connecting to DB failed.' + str(e))
            pass

    def disconnectFromDatabase(self):
        try:
            self.db.close()
            del self.db
        except AttributeError:
            pass

    def updateGlobalCount(self):
        trying = True
        trycounter = 0
        while trying:
            trycounter += 1
            try:
                # print("connecting to detabase")
                dbc = self.db.cursor()
                sql = 'SELECT globalCounter FROM cycledata ORDER BY globalCounter DESC LIMIT 1'
                dbc.execute(sql)
                variablestr = dbc.fetchone()[0]
                dbc.close()
                variablestr = variablestr.split('\n')
                variables = {}
                for line in variablestr:
                    (key, val) = line.split('\t')
                    variables[key] = float(val.strip())
                self.globalCounter = int(variables['globalCounter'])
                self.showGlobalCounter.display(self.globalCounter)

                del dbc
                trying = False

            except Exception as e:
                print("Could not fetch globalCounter. " + str(e))
                trying = True
                try:
                    self.disconnectFromDatabase()
                except:
                    pass
                try:
                    self.connectToDatabase()
                except Exception as e:
                    logging.debug(str(e))
                    pass
            if trycounter >= 10:
                raise Exception('Cannot connect to database after 10 attempts.')
                logging.WARNING('Could not fetch globalCounter from Database.')
                break

    def connectToCamera(self):
        pass


    def initCam(self):
        # Init camera

        if self.camIsConnected:
            del self.cam
            self.camIsConnected = False

        camType = self.cameraSelect.currentText()
        if camType == "Lumenera LT365RM":
            from LumeneraLT365RM import LumAcquisition
            self.cam = LumAcquisition()
            self.camIsConnected = True
            # print selected cam to rolling view in GUI
        elif camType == "PixelFly USB":
            pass
            # self.cam = PixelFly()
            # self.cam.prepareImageCapture()
            # self.camIsConnected = True
            # print selected cam to rolling view in GUI
        elif camType == "Unibrain":
            pass
            # self.cam = Unibrain()
            # print selected cam to rolling view in GUI
        else:
            pass
            # self.cam = DummyCam()
            # print selected cam to rolling view in GUI

    def cycleStarted(self):
        logging.DEBUG('New cycle started.')
        if self.isGettingImages:
            logging.WARNING('New cycle started before previous cycle had time to finish!')
            logging.WARNING('Previous cycle thread is killed. Images may be lost.')
            self.cycleFinished()

        self.startAcquisition()

    def cycleFinished(self):
        # Cleanup after each cycle
        for thread in self.threads:
            del thread
        self.localCounter = 0
        self.isGettingImages = False


    def readyForAcquisition(self):
        try:
            self.disconnectFromDatabase()
        except AttributeError:
            pass
        self.connectToDatabase()

        # Listen for trigger from Control Computer, and start the acquisition thereafter.
        networkThread = NetworkListenThread(self)
        self.communication = InstrumentServer('CameraCaptureGUI',networkThread,comms)
        self.communication.start()


    def startAcquisition(self):
        self.isGettingImages = True
        self.cam = self.initCam()
        # Grey out start- and snapshots buttons, and the camera selector.

        # Start a dedicated thread to listen for the images
        imageAcquisition = ImageCaptureThread(self.cam)
        imageAcquisition.imagesReady.connect(self.gotNewImage)
        self.threads.append(imageAcquisition)

        imageAcquisition.start()

    def gotNewImage(self, images):
        self.localCounter += 1

        # Cleanup ImAq Threads
        for thread in self.threads:
            del thread

        self.updateGlobalCount()

        self.lucam.SaveImage(images[i], "img/snap_{}-{}.tif".format(self.globalCounter, self.localCounter))
        # TODO: Figure out a way to save the metadata (might already be in database?)

        if self.localCounter >= self.numImages:
            self.cycleFinished()
            # Send socket trigger to Analysis GUI
            return

        if self.isGettingImages:
            self.startAcquisition()

    def stopAcquisition(self):
        # Stop greying out
        # Stop all acquisition threads.
        try:
            self.communication.join()
            del self.communication
        except AttributeError:
            pass

        for thread in self.threads:
            del thread

        # Disconnect from DB
        try:
            self.disconnectFromDatabase()
        except NameError:
            # DB already disconnected
            pass

        try:
            del cam
        except NameError:
            print("No camera to disconnect. Returning")
            return
        print("Camera disconnected")

    def takeSnapshot(self):
        # something like cam.snapshot() should be doable for all cameras. Check if cam initialized (include "init" flag in the cam class)
        cam = self.initCam()
        images = list()
        cam.snap(images,1)
        del cam
        return


class NetworkListenThread(threading.Thread):
    def __init__(self,mainThread):
        threading.Thread.__init__(self)
        self.mainThread = mainThread

    def run(self, t):
        self.mainThread.cycleStarted()
        print("Communications took " + str((time() - t) * 1000) + " ms")
        pass



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = ImAqGUI()
    window.show()
    sys.exit(app.exec_())