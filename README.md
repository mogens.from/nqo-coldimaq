# NQO-ColdImAq

Developed for aqcuiring images of cold atoms at the Nonlinear Quantum Optics group in SDU, Odense. The system grabs 3 images, timed by a hardware trigger, and saves them to drive, with a naming according to the global counter of the experiment. 

The system does not perform any analysis of the images.

## Requirements:
* Lumenera SDK/Drivers (Comes with the LuCam software)
* The group SVN mounted as a network drive.